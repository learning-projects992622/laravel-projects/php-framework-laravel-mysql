<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
        ];
    }

    //Relationship with Listings
    public function listings(){
        // a User has many listings
        return $this->hasMany(Listing::class,'user_id');
    }
    // after doing this we can do php artisan tinker, a tool to check the db
    // \App\Models\Listing::first() / first data din Listing
    //\App\Models\Listing::find(5) / 5th data din Listing
    //  \App\Models\Listing::find(1)->user  / specific user that owns the listing
    // all of those things are available because the relationship between the tables
    // nice tool to mess around//  php artisan tinker
}
