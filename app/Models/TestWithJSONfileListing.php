<?php 
namespace App\Models;

class TestWithJSONfileListing {
    public static function all(){
        return ['listings' => [
            'id' => 1, 
            'title'=>'Listing One',
            'description' => ' Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae doloribus cumque corporis soluta, voluptate maxime neque sequi praesentium explicabo, quos magni enim? Praesentium laborum magnam natus suscipit distinctio facere dolore.',
    ],
    [
            'id' => 2, 
            'title'=>'Listing Two',
            'description' => ' Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae doloribus cumque corporis soluta, voluptate maxime neque sequi praesentium explicabo, quos magni enim? Praesentium laborum magnam natus suscipit distinctio facere dolore.',
    ]];
    }

    public static function find($id) {
        $listings = self::all();
        foreach ($listings as $listing){
            if($listing['id'] == $id){
                return $listing;
            }
        }
    }
}




?>