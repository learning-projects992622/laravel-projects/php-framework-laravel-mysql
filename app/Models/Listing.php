<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    // Add [title] to fillable property to allow mass assignment on [App\Models\Listing].
        // we MUST DEFINE the property inside the Model. exactly what you validate in the controller-> store function 
        protected $fillable = ['user_id', 'title','logo' ,'company', 'location', 'email', 'description', 'tags', 'website'];
        // we can replace this variable with 
        // going in the App\Providers in the function boot(): void 
        // write Model::unguard();
        // With this we can replace the protected $fillable

    use HasFactory;

    public function scopeFilter($query, array $filters){
        // dd($filters['tag']); check if we succesed to stock the data inside the $filters array
        if ($filters['tag'] ?? false) { // ?? null call operator
            $query->where('tags', 'like', '%' . request('tag') . '%'); // sql query condition, works perfect
            // basically Listing::latest()->filter(request(['tag']))->get()]) is stocked inside array $filters
            // filter but only with $query condition 
        }
        // Step 3/ search bar /// in the same way 
        //Remember that 'search' is the name of the input
        // Because we have a filter method request inside the controller we can use for next filter, because everything is a filter
        if ($filters['search'] ?? false) { // ?? null call operator
            // decide where you want to search
            $query->where('title', 'like', '%' . request('search') . '%') // title is where we want to search for name
            ->orWhere('description', 'like', '%' . request('search') . '%') // diferent where/ condition to search inside the db
            ->orWhere('tags', 'like', '%' . request('search') . '%'); // multiple condition for searching
            // That means we can have diferent condition to search in the database
        }
    }

    // Relationship to User
    public function user(){
        // Listing model
        // a listing belongs to a user 
        return $this->belongsTo(User::class, 'user_id');
    } // doing the same for the User model!!! New thing
    
}
