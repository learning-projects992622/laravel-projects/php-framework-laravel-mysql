<?php

namespace App\Http\Controllers;

use App\Models\Listing;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


// Controller was created with php artisan make:controller ListingController

class ListingController extends Controller
{   
    //Show all listings
    public function index(){
        // dd(request('tag'));
        // we return the view listings.index because index blade is inside the folder listings
        return view('listings.index', ['listings' => Listing::latest()->filter(request(['tag','search']))->paginate(6)]); // In stade of all() we can do latest()->get() and it will be sorted after the last one
        // after creating the scopeFilter from Listing  Model we can filter using the filter method, inside all the listing we cand filter the request of ['tag'], trebuie sa fie ARRAY

        //SEARCH bar
        // Step 1/ add in the ->filter(request(['tag','search'])) another element, 'search'
        // Step 2/ after go in the Model

        //paginate()
        // In stade of ->get() we can use paginate(inside how many elements we want on the page)
        // php artisan vendor:publish for more paginate templates!!!
    }
    //Show single listing
    public function show(Listing $listing){
        // we return the view listings.show because show blade is inside the folder listings
        return view('listings.show', ['listing' =>$listing]);
    }
    // show the create form
    public function create(){
        return view('listings.create');
    }
    // Store data
    public function store(Request $request){
        // dd($request->all()); check the data before starting the code
       
        // dd($request->file('logo'));
        // for the file input we go in the config/filesystems.php and modifie the default from local to public

         // MUST validate!!!!
        $formFields = $request->validate([
            'title' => 'required',
            // 'logo' => ['required','image','mimes:jpeg,png,jpg,gif','max:2048'],
            'company' => ['required', Rule::unique('listings','company')], // Rule::unique('listings', 'company') unique() has 2 parameters, 1st is the name of the table, 2nd is the name of the column
            'location' => 'required',
            'website' => 'required',
            'email' => ['required', 'email'],
            'tags' => 'required',
            'description' => 'required',
        ]);
        //upload image
        // remember that you are using the Model Listing.php protected $fillable 
        // you must add the new column name there!!!
        if($request->hasFile('logo')){
            $formFields['logo'] = $request->file('logo')->store('logos', 'public'); // $protected fillable  in the model Listing!!!!
        } //  you should create a symbolic link from public/storage to storage/app/public
        // with: php artisan storage:link
        // after you can see the image writing the path to the image in your url 
        // http://127.0.0.1:8000/storage/logos/ZLxKfgOdn7tpYkROEODSnyqtB7uFg6NeWCQ8GeWq.png
        // display the picture in the website

        // Because of the new relationship between the tables we must do here/ create the new column 'user_id'
        $formFields['user_id'] = auth()->id(); // we are seting the listings.user_id with the user.id
        // protected $fillable in the model Listing!!!!

        //For creating data we need only Listing::create($formFields)
        Listing::create($formFields); 
        // after we fill all the input we have this error
        // Add [title] to fillable property to allow mass assignment on [App\Models\Listing].
        // we MUST DEFINE the property inside the Model.

        //we are using the redirect because when we create a new data it will redirect to the home page!!!
        // we can also redirect with a message!!!
        return redirect('/')->with('message', 'Listing created successfully!!'); // but like this will not show the message, we will create a diferent component flash-message.blade.php for this!!
    } 

    // UPDATE/EDIT
    // 1. show edit form
    public function edit(Listing $listing) {
        // check if you have data dd($listing);
        return view('listings.edit', ['listing' => $listing]);
    }
    // 2. Update/ show method
    public function update(Request $request, Listing $listing){
        // Make sure logged in user is the owner
        if($listing->user_id !== auth()->id()){
            abort(403,' Unauthorized Action');
        }

        //Comments in the store function
        $formFields = $request->validate([
            'title' => 'required',
            // 'logo' => ['required','image','mimes:jpeg,png,jpg,gif','max:2048'],
            'company' => 'required', // Don't need this Rule::unique('listings','company') because will interact badly with the update
            'location' => 'required',
            'website' => 'required',
            'email' => ['required', 'email'],
            'tags' => 'required',
            'description' => 'required',
        ]);
       
        if($request->hasFile('logo')){
            $formFields['logo'] = $request->file('logo')->store('logos', 'public');
        } 
        // because we need the actually listing we don't use the model  Listing::, we update the curent $listing
        //  $listing->update($formFields);
        $listing->update($formFields); 
        return back()->with('message', 'Listing updated successfully!!');
    }

     //DELETE
     public function destroy(Listing $listing){
        // Make sure logged in user is the owner
        if($listing->user_id !== auth()->id()){
            abort(403,' Unauthorized Action');
        }
        
        $listing->delete();
        return redirect('/')->with('message', 'Listing deleted successfully!!!');
    }

    //Manage Listings
    public function manage(){
        return view('listings.manage', ['listings' => auth()->user()->listings()->get()]);
    }
}
