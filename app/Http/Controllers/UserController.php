<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    //Show Register/Create form
    public function create(){
        return view('users.register');
    }

    //Create new users
    public function store(Request $request){
        // store the validation in a variable
        $formFields = $request->validate([
            'name' => ['required', 'min:3'],
            'email' => ['required', 'email', Rule::unique('users', 'email')],
            'password' => ['required', 'confirmed','min:6'], //confirmed is the convention that can confirm another password field passord_confirmation
        ]);

        // Hashed Password!!!!
        $formFields['password'] = bcrypt($formFields['password']);

        // create user
        $user = User::create($formFields);

        // login direct
        auth()->login($user);

        //redirect home page logged and message 
        return redirect('/')->with('message', 'User created and logged in');
    }

    // Logout User
    public function logout(Request $request){
        //call auth()
        //will remove the authentication information from the user session
        auth()->logout();

        // is recomandate to invalidate the user session and regenerate Token 
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        // redirect to home page with message
        return redirect('/')->with('message', 'You have been logout!');
    }

    //Show Login Form
    public function login(){
        return view('users.login');
    }

    //Authenticate user
    public function authenticate(Request $request){
        // for loggin we need to validate one's again the email and password
        $formFields = $request->validate([
            'email' => ['required', 'email'], //Rule::unique('users', 'email'),
            'password' => 'required', // [ , 'confirmed','min:6']
        ]);
        // we need to attemp to login the user
        if (auth()->attempt($formFields)) {
            $request->session()->regenerate();

            // we try to return if is true
            return redirect('/')->with('message', 'You are now logged in!!');
        }
        // else return back
        return back()->withErrors(['email' => 'Invalid Credentials!'])->onlyInput('email');
    }
}
