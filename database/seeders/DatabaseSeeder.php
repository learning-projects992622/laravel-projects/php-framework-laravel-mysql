<?php

namespace Database\Seeders;

use App\Models\Listing;
use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        //We need to create a connection between the tables Users and Listings
        //store in a variable
        $user = User::factory()->create(['name' => "Jhon Doe",
        'email' => "john@gmail.com"]);
        //after doing the connection we need to do it inside the Model also

        // Listing::factory(6)->create(['user_id' => $user->id]); OR THIS FOR FAKE!!!
        // I created new factory php artisan make:factory ListingFactory
        // Didn't tested, but i thing it's work

        // User::factory(10)->create(); commented this because we want to create a connection between the tables
        // php artisan db:seed
        // create directly 10 fake data inside user table
        // WONDERFUL !!!!!!
        // if you want to refresh the database, remove the fake data 
        //php artisan migrate:refresh / fresh

        // User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        //php artisan migrate:refresh --seed 
        //represh and create new fake data
        Listing::create(["title" => "Senior Software Engineer",
            'user_id' => $user->id, // we add the new column and it's value is the id that takes from Users!!!!
            "tags" => "Engineering, Tech",
            "company" => "TechCorp Inc.",
            "location" => "San Francisco, CA",
            "email" => "jobs@techcorp.com",
            "website" => "www.techcorp.com",
            "description" => "Develop and maintain software applications for various clients."
        ]);
        Listing::create(["title" => "Marketing Specialist",
            'user_id' => $user->id,
            "tags" => "Marketing, Sales",
            "company" => "MarketMasters",
            "location" => "New York, NY",
            "email" => "careers@marketmasters.com",
            "website" => "www.marketmasters.com",
            "description" => "Design and implement marketing strategies to increase brand awareness."
        ]);
        Listing::create(["title" => "Product Manager",
            'user_id' => $user->id,
            "tags" => "Management, Product",
            "company" => "InnovateTech",
            "location" => "Austin, TX",
            "email" => "hr@innovatetech.com",
            "website" => "www.innovatetech.com",
            "description" => "Oversee product development from ideation to launch and beyond."
        ]);
        Listing::create(["title" => "Graphic Designer",
            'user_id' => $user->id,
            "tags" => "Design, Creative",
            "company" => "DesignWorks",
            "location" => "Los Angeles, CA",
            "email" => "design@designworks.com",
            "website" => "www.designworks.com",
            "description" => "Create visual content for print and digital media, ensuring brand cohesion."
        ]);
        Listing::create(["title" => "Data Analyst",
            'user_id' => $user->id,
            "tags" => "Data, Analytics",
            "company" => "DataInsight",
            "location" => "Chicago, IL",
            "email" => "analyst@datainsight.com",
            "website" => "www.datainsight.com",
            "description" => "Analyze and interpret complex data sets to help drive business decisions."
        ]);
        Listing::create(["title" => "HR Coordinator",
            'user_id' => $user->id,
            "tags" => "HR, Recruitment",
            "company" => "PeoplePros",
            "location" => "Miami, FL",
            "email" => "hr@peoplepros.com",
            "website" => "www.peoplepros.com",
            "description" => "Assist in the recruitment process and manage employee relations."
        ]);
        Listing::create(["title" => "Sales Executive",
            'user_id' => $user->id,
            "tags" => "Sales, Business",
            "company" => "SalesGurus",
            "location" => "Dallas, TX",
            "email" => "sales@salesgurus.com",
            "website" => "www.salesgurus.com",
            "description" => "Develop and maintain client relationships to drive sales and revenue."
        ]);
        Listing::create(["title" => "Customer Support Rep",
            'user_id' => $user->id,
            "tags" => "Support, Customer",
            "company" => "HelpDesk Inc.",
            "location" => "Seattle, WA",
            "email" => "support@helpdeskinc.com",
            "website" => "www.helpdeskinc.com",
            "description" => "Provide customer support and troubleshoot issues via phone and email."
        ]);
        Listing::create(["title" => "Content Writer",
            'user_id' => $user->id,
            "tags" => "Writing, Content",
            "company" => "WriteRight",
            "location" => "Boston, MA",
            "email" => "jobs@writeright.com",
            "website" => "www.writeright.com",
            "description" => "Produce engaging and informative content for various platforms and clients."
        ]);
        Listing::create(["title" => "UX/UI Designer",
            'user_id' => $user->id,
            "tags" => "Design, Tech",
            "company" => "UserFirst",
            "location" => "Denver, CO",
            "email" => "design@userfirst.com",
            "website" => "www.userfirst.com",
            "description" => "Design user interfaces and experiences that are intuitive and engaging."
        ]);
    }
}
