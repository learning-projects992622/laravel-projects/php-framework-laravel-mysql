<?php
$listings = [
    [
        "title" => "Senior Software Engineer",
        "tags" => "Engineering, Tech",
        "company" => "TechCorp Inc.",
        "location" => "San Francisco, CA",
        "email" => "jobs@techcorp.com",
        "website" => "www.techcorp.com",
        "description" => "Develop and maintain software applications for various clients."
    ],
    [
        "title" => "Marketing Specialist",
        "tags" => "Marketing, Sales",
        "company" => "MarketMasters",
        "location" => "New York, NY",
        "email" => "careers@marketmasters.com",
        "website" => "www.marketmasters.com",
        "description" => "Design and implement marketing strategies to increase brand awareness."
    ],
    [
        "title" => "Product Manager",
        "tags" => "Management, Product",
        "company" => "InnovateTech",
        "location" => "Austin, TX",
        "email" => "hr@innovatetech.com",
        "website" => "www.innovatetech.com",
        "description" => "Oversee product development from ideation to launch and beyond."
    ],
    [
        "title" => "Graphic Designer",
        "tags" => "Design, Creative",
        "company" => "DesignWorks",
        "location" => "Los Angeles, CA",
        "email" => "design@designworks.com",
        "website" => "www.designworks.com",
        "description" => "Create visual content for print and digital media, ensuring brand cohesion."
    ],
    [
        "title" => "Data Analyst",
        "tags" => "Data, Analytics",
        "company" => "DataInsight",
        "location" => "Chicago, IL",
        "email" => "analyst@datainsight.com",
        "website" => "www.datainsight.com",
        "description" => "Analyze and interpret complex data sets to help drive business decisions."
    ],
    [
        "title" => "HR Coordinator",
        "tags" => "HR, Recruitment",
        "company" => "PeoplePros",
        "location" => "Miami, FL",
        "email" => "hr@peoplepros.com",
        "website" => "www.peoplepros.com",
        "description" => "Assist in the recruitment process and manage employee relations."
    ],
    [
        "title" => "Sales Executive",
        "tags" => "Sales, Business",
        "company" => "SalesGurus",
        "location" => "Dallas, TX",
        "email" => "sales@salesgurus.com",
        "website" => "www.salesgurus.com",
        "description" => "Develop and maintain client relationships to drive sales and revenue."
    ],
    [
        "title" => "Customer Support Rep",
        "tags" => "Support, Customer",
        "company" => "HelpDesk Inc.",
        "location" => "Seattle, WA",
        "email" => "support@helpdeskinc.com",
        "website" => "www.helpdeskinc.com",
        "description" => "Provide customer support and troubleshoot issues via phone and email."
    ],
    [
        "title" => "Content Writer",
        "tags" => "Writing, Content",
        "company" => "WriteRight",
        "location" => "Boston, MA",
        "email" => "jobs@writeright.com",
        "website" => "www.writeright.com",
        "description" => "Produce engaging and informative content for various platforms and clients."
    ],
    [
        "title" => "UX/UI Designer",
        "tags" => "Design, Tech",
        "company" => "UserFirst",
        "location" => "Denver, CO",
        "email" => "design@userfirst.com",
        "website" => "www.userfirst.com",
        "description" => "Design user interfaces and experiences that are intuitive and engaging."
    ]
];