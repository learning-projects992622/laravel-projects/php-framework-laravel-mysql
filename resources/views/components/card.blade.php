{{-- Here the attribute will have by default these style, BUT will merge every change that you make--}}
{{-- In theory works, but now ....  --}}
<div {{ $attributes->merge(['class'=>'bg-gray-50 border border-gray-200 rounded p-6']) }}>
    {{-- Normally the component is self-closing
        But if you want to put something inside, put {{$slot}} 
        Anything you inside the component tag, it will be inside the {{$slot}}--}}
        {{$slot}}
</div>