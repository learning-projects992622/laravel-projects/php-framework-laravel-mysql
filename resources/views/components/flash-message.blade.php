{{-- here we create were the message must appear and style --}}
@if (session()->has('message'))
    {{-- alpinejs.dev
         x-data="{show: true}" and because we want that 
    the message stay only a few seconds x-init="setTimeout(()=>show = false, 3000),
    when it initializat x-init ="" setTimeout() which takes 2 parameters 1st is an arrow function show=false and the 2nd time 3000, 
    set to false after 3 seconds 
    AND when is finished, we set the show to true again x-show="show"--}}
    <div x-data="{show: true}" x-init="setTimeout(()=>show = false, 3000)" x-show="show" 
        class="fixed top-0 left-1/2 transform -translate-x-1/2 bg-[#ef3b2d] text-white px-48 py-3">
        <p>
            {{session('message')}}
        </p>
    </div>
@endif
{{-- after we must put it in the layout.blade.php were we want because the message has a fixed position --}}