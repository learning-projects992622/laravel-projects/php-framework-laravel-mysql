@props(['listing'])
{{-- Because we use {{$slot}}, everything will be inside card.blade.php --}}
<x-card>
    <div class="flex">
      {{-- using an ternory expression because we want that if we have an image to show but if not will display the no-image picture --}}
      <img 
      class="hidden w-48 mr-6 md:block" 
      src="{{$listing->logo ? asset('storage/'. $listing->logo) : asset('/images/no-image.png')}}" 
      alt="No image">
      <div>
        <h3 class="text-2xl">
          <a href="/listings/{{$listing->id }}">{{ $listing->title}}</a>
        </h3>
        <div class="text-xl font-bold mb-4">{{ $listing->company}}</div>

        {{-- we are passing the listings-tags component with the attribute $tagsCsv 
        the attribute is equal with the column name :tagsCsv="$listing->tags--}}
        <x-listing-tags :tagsCsv="$listing->tags"/>
            
        <div class="flex text-lg my-4">
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
            <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1 1 15 0Z" />
          </svg>
          {{$listing->location}}
        </div>
      </div>
    </div>
</x-card>