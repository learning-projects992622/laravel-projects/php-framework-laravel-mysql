{{-- luam categoriile din baza de date ca si props!!! si le dam un nume si ca este o lista @props(['tagsCsv']) --}}
@props(['tagsCsv'])
{{-- return into array for looping inside --}}
@php
// inside php tag we will create a variable for storing the data
    $tags = explode(',', $tagsCsv); // we will use explode function wich will take the first parameter were we want to split the text ',' and were we want to search
    // that means it will transform the text from the column in an array which will be store inside $tags
@endphp
<ul class="flex">
    {{--after storing the array inside the variable $tags
     Create a foreach loop for displaing the tags --}}
    @foreach ($tags as $tag)
        <li class="felx items-center justify-center bg-black text-white rounded-xl py-1 px-3 mr-2 text-xs">
            {{-- we want to filter the tags by that tag /?tag={{$tag}} --}}
            <a href="/?tag={{$tag}}">{{$tag}}</a>
        </li>
    @endforeach
  </ul>
  {{-- now replace the component were you need
    In our case inside the listing-card.blade.php and listing.blade.php
    !!!! REMEBER now you have the $tagsCsv attribute/props !!!!--}}