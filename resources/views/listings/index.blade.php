{{-- Layout component --}}
<x-layout>
{{-- Because extends the layout file 
  After starting using the component file, don't need to use @extends or @section--}}
{{-- Commented @extends('layout') because we moved the file inside the components folder--}}

{{-- Inside the layout we have @yield('content') --}}
{{-- Commented @section('content') because we moved the file inside the components folder --}}
@include('partials._hero')
@include('partials._search')
      {{-- <h1 class="text-3xl font-bold underline">
        {{$heading}}
      </h1>
        <h2 class="text-xl font-bold">
             the href from Route,, only replace the {id} with the id from the ass array 
          <a href="/listings/{{$listing['id']}}">{{ $listing['title'] }}</a>
        </h2>
          <p>{{ $listing['description'] }}</p> --}}
  <div class="lg:grid lg:grid-cols-2 gap-4 space-y-4 md:space-y-0 mx-4">
    @unless (count($listings) == 0)
      @foreach ($listings as $listing)
      {{-- props :listing="$listing" --}}
        <x-listing-card :listing="$listing"/>
      @endforeach
    @else
      <p>No listings found</p>
    @endunless
  </div>
  {{-- for the paginate we need to change the pages  --}}
  <div class="mt-6 p-4">
    {{ $listings->links() }}
    {{-- because we are using the Tailwind, the numbers of the page in the right side to appear needs this ->
      './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
      inside the tailwind.confing.js --}}
  </div>
{{-- Commented @endsection because we moved the file inside the components folder--}}
</x-layout>