{{-- Layout component --}}
<x-layout>
{{-- Because extends the layout file 
  After starting using the component file, don't need to use @extends or @section--}}
{{-- Commented @extends('layout') because we moved the file inside the components folder--}}

{{-- Inside the layout we have @yield('content') --}}
{{-- Commented @section('content') because we moved the file inside the components folder --}}
    @include('partials._search')
        {{-- <h2>{{ $listing['title'] }}</h2></a>
        <p>{{ $listing['description'] }}</p> --}}
        <a href="/" class="flex text-black ml-4 mb-4">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="M9 15 3 9m0 0 6-6M3 9h12a6 6 0 0 1 0 12h-3" />
            </svg>Back      
        </a>
        <div class="mx-4">
            {{-- We can not change the style from here !!!
                It has diferent padding, create the variable $attribute inside the card.blade.php and after you can change the class style --}}
            <x-card class="p-10">
                <div class="flex flex-col items-center text-center">
                    <img class="w-28 mr-6 mb-6" src="{{$listing->logo ? asset('storage/'. $listing->logo) : asset('/images/no-image.png')}}" alt="">
                    <h3 class="text-2xl mb-2">
                        {{ $listing->title }}
                    </h3>
                    <div class="text-xl font-bold mb-4">{{ $listing->company}}</div>
                    {{-- we are passing the listings-tags component with the attribute $tagsCsv 
                        the attribute is equal with the column name :tagsCsv="$listing->tags--}}
                    <x-listing-tags :tagsCsv="$listing->tags"/>
                    <div class="flex text-lg my-4">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                        <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1 1 15 0Z" />
                        </svg>
                        {{$listing->location}}
                    </div>
                    <div class="border border-gray-200 w-full mb-6"></div>
                    <div>
                        <h3 class="text-3xl fond-bold mb-4">Job Description</h3>
                        <div class="text-lg space-y-6">
                            <p>{{$listing->description}}</p>
                            <a href="mailto:{{$listing->email}}" class="flex bg-[#ef3b2d] text-white mt-6 py-2 rounded-xl hover:opacity-80 items-center justify-center">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6 flex">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M21.75 6.75v10.5a2.25 2.25 0 0 1-2.25 2.25h-15a2.25 2.25 0 0 1-2.25-2.25V6.75m19.5 0A2.25 2.25 0 0 0 19.5 4.5h-15a2.25 2.25 0 0 0-2.25 2.25m19.5 0v.243a2.25 2.25 0 0 1-1.07 1.916l-7.5 4.615a2.25 2.25 0 0 1-2.36 0L3.32 8.91a2.25 2.25 0 0 1-1.07-1.916V6.75" />
                            </svg>Contact Employer</a>
                            <a href="{{$listing->website}}" target="_blank" class="flex bg-black text-white py-2 rounded-xl items-center justify-center">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M12 21a9.004 9.004 0 0 0 8.716-6.747M12 21a9.004 9.004 0 0 1-8.716-6.747M12 21c2.485 0 4.5-4.03 4.5-9S14.485 3 12 3m0 18c-2.485 0-4.5-4.03-4.5-9S9.515 3 12 3m0 0a8.997 8.997 0 0 1 7.843 4.582M12 3a8.997 8.997 0 0 0-7.843 4.582m15.686 0A11.953 11.953 0 0 1 12 10.5c-2.998 0-5.74-1.1-7.843-2.918m15.686 0A8.959 8.959 0 0 1 21 12c0 .778-.099 1.533-.284 2.253m0 0A17.919 17.919 0 0 1 12 16.5c-3.162 0-6.133-.815-8.716-2.247m0 0A9.015 9.015 0 0 1 3 12c0-1.605.42-3.113 1.157-4.418" />
                                </svg>Visit Website</a>
                        </div>
                    </div>
                </div>
            </x-card>
            @auth
                <x-card class="flex mt-4 p-2 space-x-6">
                    <a class="flex" href="/listings/{{$listing->id}}/edit"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="size-6">
                        <path d="M21.731 2.269a2.625 2.625 0 0 0-3.712 0l-1.157 1.157 3.712 3.712 1.157-1.157a2.625 2.625 0 0 0 0-3.712ZM19.513 8.199l-3.712-3.712-12.15 12.15a5.25 5.25 0 0 0-1.32 2.214l-.8 2.685a.75.75 0 0 0 .933.933l2.685-.8a5.25 5.25 0 0 0 2.214-1.32L19.513 8.2Z" />
                    </svg>
                    Edit</a>
                    <form method="POST" action="/listings/{{$listing->id}}">
                        @csrf
                        {{-- because to update something we need DELETE method, but the form accept only GET or POST
                        we need to use @method('DELETE') laravelFramwork tool --}}
                        @method('DELETE')
                        <button class="flex text-red-500"type="submit">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                            </svg>
                        Delete</button>
                    </form>
                </x-card>
            @endauth
        </div>
    {{-- Commented @endsection because we moved the file inside the components folder--}}
</x-layout>