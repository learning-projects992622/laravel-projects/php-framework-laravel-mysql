<?php

use App\Http\Controllers\ListingController;
use App\Http\Controllers\UserController;
use App\Models\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\TestWithJSONfileListing;
//Naming convention
// index - show all listings
// show - show single listing
// create - show form to create new listing
// store - store new listing
// edit - show form to edit listing
// update - update listing
// destroy - delete listing


//show all 
Route::get('/',[ ListingController::class,'index']);

//show create form/ THE ROUTE MUST HAVE AN ORDER
//we moved the Route because when press the button it read the /listings/{listing} url 
Route::get('/listings/create',[ListingController::class,'create'])->middleware('auth'); // using middleware('auth'). by default is working, pressing the buttton post you will have an error "Route [login] not defined.". For fixing this we are going to the route with login because it needs an name!!!

// store listing data
Route::post('/listings',[ ListingController::class,'store'])->middleware('auth');

//Update
// 1. Show edit Form
Route::get('/listings/{listing}/edit', [ListingController::class,'edit'])->middleware('auth');
// 2. Edit/Update 
Route::put('/listings/{listing}', [ListingController::class, 'update'])->middleware('auth');

//DELETE
Route::delete('/listings/{listing}', [ListingController::class, 'destroy'])->middleware('auth');

// Manage Listings
Route::get('/listings/manage',[ListingController::class, 'manage'])->middleware('auth');

// show single listing, keep the method Eloquent Model
Route::get('/listings/{listing}',[ ListingController::class,'show']);



//Register
// 1. Show Register/Create Form
Route::get('/register', [UserController::class, 'create'])->middleware('guest'); // 1. we add GUEST because we want the register form to be acces only by guests!!! / 2. every time when we try to acces a view by URL we will be redirected to home by default

// 2. Create new user
Route::post('/users',[UserController::class,'store']);

//Log User out
// after need an logout function with a Request
Route::post('/logout', [UserController::class,'logout'])->middleware('auth');

//show login form
Route::get('/login', [UserController::class,'login'])->name('login')->middleware('guest'); //1. ->name('login') here is the name, after this the authentification is mandatory. You must be login to post an gig /2. ->middleware('guest'); because we want the login form to be acces only by guests!!! / 3. every time when we try to acces a view by URL we will be redirected to home by default

// Login In User
Route::post('/users/authenticate', [UserController::class,'authenticate']);



// i commented when i created the controller ListingController
//show all listings
// Route::get('/', function () {
//     return view('listings', ['listings' => Listing::all()]); // Nu trebuie sa schimbam nimic all() exista in Eloquent
// });
// Case 1 Normal

// show one listing
// Route::get('/listings/{id}', function($id){ 
//     return view('listing',
//     ['listing' =>Listing::find($id)]); //find la fel, exista in Eloquent
// });

// Case 2  Route model binding

// 
// Route::get('/listings/{id}', function($id){ // In cazul de mai sus daca trecem un alt id care nu exista in db, vom avea o eroare
//     // putem creea un if-else ca sa nu avem eroare ci un mesaj 404 Not found
//     //putem face asa
//     $listing = Listing::find($id);

//     if ($listing){
//         return view('listing',
//     ['listing' =>$listing]);
//     } else {
//         abort('404');
//     }
// });

// Case 3 Since we used Eloquent Model "Best"
// it was commented when i created the controller ListingController
// show one listing
// Route::get('/listings/{listing}', function( Listing $listing){ // Inlocuim {id} cu lista noastra {listing} si ca parametru in functie () trecem Model: Listing $listing

// // $listing este exact {listing} din url

//     // $listing = Listing::find($id); NU mai trebuie sa folosim find($id) pentru ca avem deja lista in $listing

//     //NU mai trebuie sa facem acest if-else deoarece are acest 404 implementat automat
//     // if ($listing){
//     //     return view('listing',
//     // ['listing' =>$listing]);
//     // } else {
//     //     abort('404');
//     // }
//     return view('listing',
//         ['listing' =>$listing]);
// });

/* 
Used with TestWithJSONfileListing JSON 
//all listing
Route::get('/', function () {
    // had problem putting everything inside the return, create first the ass array and after puting in the view, works perfect
   
    return view('listings', [
        'heading' => 'Latest Listings', 'listings' => TestWithJSONfileListing::all()]); // functia din modelul Listing
});
// single listing
    Route::get('/listings/{id}', function($id){ // url must be the same with the <a hre=""> 
        return view('listing',
        ['listing' =>TestWithJSONfileListing::find($id)]); // this is the function from the model!!!
    });
    */
/* 
// test things with the route
Route::get('/hello', function(){
    return response("<h1>Hello World!!</h1>", 200)
    ->header('Content-Type', 'text/plain')
    ->header('foo', 'bar');
});
*/

/* 
Route::get('/posts/{id}', function($id){
    // dd($id); for debuging purpose
    return response('Post' . $id);
    //Regular Expression Constraints, trebuie sa fie cand se inchide Route
})->where('id', '[0-9]+'); // conditia sa fie numere intre 0-9 ;
*/

/*
// my URL http://127.0.0.1:8000/search?name=Flavian&city=Bereldange
Route::get('/search', function (Request $request){
    // dd($request->name . ' ' . $request->city);
    return  $request->name . ' ' . $request->city; // extrag informatiile din URL cu Request $request
});
*/