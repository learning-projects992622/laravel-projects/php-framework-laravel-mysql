# PHP framework Laravel-MySQL 

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Name
**LaravelTailPro**

Laravel: Highlights the PHP framework being used.
Tail: Represents Tailwind CSS.
Pro: Indicates professionalism and project-oriented functionality.

## Description
Learn the Laravel PHP framework from scratch by building a job listings application with Laravel and MySQL.


## Installation
1. Prerequisites
Before using LaravelTailPro, ensure you have the following installed:

PHP >= 8.0
Composer
MySQL
Node.js & npm

**Installation**
2. Clone the repository:
git clone https://github.com/yourusername/LaravelTailPro.git
cd LaravelTailPro

3. Install dependencies:
composer install
npm install
npm run dev

4. Set up environment variables:
cp .env.example .env
php artisan key:generate
php artisan config:cache

5. Configure the .env file with your database credentials:
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=your_database
DB_USERNAME=your_username
DB_PASSWORD=your_password

6. Run database migrations:
php artisan migrate

7. Serve the application:
php artisan serve


## Usage
**User Registration**
To start using the application, users need to register.
Fill in the registration form with your details.
Click "Sign up".

**Creating a Post**
Users can create posts for jobs or projects.

**Viewing Posts**
Users can view all the available posts.

**Updating a Post**
Users can update their own posts.

**Deleting a Post**
Users can delete their own posts.
